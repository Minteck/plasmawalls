<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/errors.php"; $_TITLE = "Where all Plasma wallpapers meet"; require_once $_SERVER['DOCUMENT_ROOT'] . "/private/header.php"; ?>

<?php

function getnames($branch) {
    global $_KEY;

    $inf = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/cache/branch-" . str_replace("/", "_-_", $branch) . ".json"));

    $names = [];

    foreach ($inf as $file) {
        if (substr($file->name, 0, 10) != "base_size." && substr($file->name, 0, 19) != "vertical_base_size.") {
            array_push($names, $file->name);
        }
    }

    return $names;
}

function getext($branch) {
    global $_KEY;

    $inf = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/cache/branch-" . str_replace("/", "_-_", $branch) . ".json"));

    $names = [];

    foreach ($inf as $file) {
        if (substr($file->name, 0, 10) != "base_size." && substr($file->name, 0, 19) != "vertical_base_size.") {
            array_push($names, $file->name);
        }
    }

    $ext = explode(".", $names[0])[count(explode(".", $names[0]))-1];
    return $ext;
}

?>

<div id="intro">
    <div id="intro-inner">
        <div>
            <img src="/logo.svg" height="128px" width="128px">
            <h1>PlasmaWalls</h1>
            <p>Easily download and explore KDE Plasma's default wallpapers</p>
        </div>
    </div>
</div>

<div id="lasts">
    <div id="last-0" class="last">
        <div class="last-cell">
            <div>
                <img src="/preview.php?v=last" height="200px">
            </div>
        </div>
        <div class="last-cell">
            <div>
                <h1>Latest Development Branch</h1>
                <p>Available in <b><?= count(getnames("master")) ?> different resolutions</b>, <b><?= strtoupper(getext("master")) ?> format</b></p>
                <a type="button" class="btn btn-primary" href="/wall/?v=last">Download</a>
                <a type="button" class="btn btn-secondary"target="_blank" href="https://invent.kde.org/plasma/breeze/-/tree/master/wallpapers/Next/contents/images">Source</a>
            </div>
        </div>
    </div>
    <?php

    $branches_raw = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/cache/branches.json"));

    $branches_names = [];
    foreach ($branches_raw as $branch) {
        if (substr($branch->name, 0, 7) == "Plasma/") {
            array_push($branches_names, $branch->name);
        }
    }

    sort($branches_names, SORT_NATURAL);
    $branches_names = array_reverse($branches_names);

    ?>
    <div id="last-1" class="last">
        <div class="last-cell">
            <div>
                <h1>Plasma <?= substr($branches_names[0], 7) ?></h1>
                <p>Available in <b><?= count(getnames($branches_names[0])) ?> different resolutions</b>, <b><?= strtoupper(getext($branches_names[0])) ?> format</b></p>
                <a type="button" class="btn btn-primary" href="/wall/?v=<?= substr($branches_names[0], 7) ?>">Download</a>
                <a type="button" class="btn btn-secondary"target="_blank" href="https://invent.kde.org/plasma/breeze/-/tree/<?= $branches_names[0] ?>/wallpapers/Next/contents/images">Source</a>
            </div>
        </div>
        <div class="last-cell">
            <div>
                <img src="/preview.php?v=<?= substr($branches_names[0], 7) ?>" height="200px">
            </div>
        </div>
    </div>
    <div id="last-2" class="last">
        <div class="last-cell">
            <div>
                <img src="/preview.php?v=<?= substr($branches_names[1], 7) ?>" height="200px">
            </div>
        </div>
        <div class="last-cell">
            <div>
                <h1>Plasma <?= substr($branches_names[1], 7) ?></h1>
                <p>Available in <b><?= count(getnames($branches_names[1])) ?> different resolutions</b>, <b><?= strtoupper(getext($branches_names[1])) ?> format</b></p>
                <a type="button" class="btn btn-primary" href="/wall/?v=<?= substr($branches_names[1], 7) ?>">Download</a>
                <a type="button" class="btn btn-secondary"target="_blank" href="https://invent.kde.org/plasma/breeze/-/tree/<?= $branches_names[1] ?>/wallpapers/Next/contents/images">Source</a>
            </div>
        </div>
    </div>
    <div id="last-3" class="last">
        <div class="last-cell">
            <div>
                <h1>Plasma <?= substr($branches_names[2], 7) ?></h1>
                <p>Available in <b><?= count(getnames($branches_names[2])) ?> different resolutions</b>, <b><?= strtoupper(getext($branches_names[2])) ?> format</b></p>
                <a type="button" class="btn btn-primary" href="/wall/?v=<?= substr($branches_names[2], 7) ?>">Download</a>
                <a type="button" class="btn btn-secondary"target="_blank" href="https://invent.kde.org/plasma/breeze/-/tree/<?= $branches_names[2] ?>/wallpapers/Next/contents/images">Source</a>
            </div>
        </div>
        <div class="last-cell">
            <div>
                <img src="/preview.php?v=<?= substr($branches_names[2], 7) ?>" height="200px">
            </div>
        </div>
    </div>
    <div id="last-4" class="last">
        <div class="last-cell">
            <div>
                <img src="/preview.php?v=<?= substr($branches_names[3], 7) ?>" height="200px">
            </div>
        </div>
        <div class="last-cell">
            <div>
                <h1>Plasma <?= substr($branches_names[3], 7) ?></h1>
                <p>Available in <b><?= count(getnames($branches_names[3])) ?> different resolutions</b>, <b><?= strtoupper(getext($branches_names[3])) ?> format</b></p>
                <a type="button" class="btn btn-primary" href="/wall/?v=<?= substr($branches_names[3], 7) ?>">Download</a>
                <a type="button" class="btn btn-secondary"target="_blank" href="https://invent.kde.org/plasma/breeze/-/tree/<?= $branches_names[3] ?>/wallpapers/Next/contents/images">Source</a>
            </div>
        </div>
    </div>
    <p style="text-align:center;">View older wallpapers in the top menu</p>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/private/footer.php"; ?>
